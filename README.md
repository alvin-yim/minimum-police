# Minimum Police

[![Build Status](https://semaphoreci.com/api/v1/alvin-yim/minimum-police/branches/master/badge.svg)](https://semaphoreci.com/alvin-yim/minimum-police)

The goal of this project is to create an IAM group which has view-only access
and CloudFormation access.  This IAM group is assigned to the developers so
that they can use the AWS resources through CloudFormation.
